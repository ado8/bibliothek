package com.doerzbach;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BibliothekTest {
    Bibliothek b=new Bibliothek();


    @Test
    void addMedium() {
        Medium m=new Buch("Test1","Peter Muster");
        b.addMedium(m);
        // Postitiv Testcase
        assertEquals(m,b.getMedium(m.getTitle()),"Not the right medium returned after Adding");
        // Negativ Testcase
        assertNull(b.getMedium("basdfadfadfasfdasdf"));
        // Same title should return the right object to but it does not -> Title needs to be unique
        Film m1=new Film("Test1");
        b.addMedium(m1);
        //assertEquals(m1,b.getMedium(m1.getTitle()),"Not the right medium returned after Adding");

    }

    @Test
    void loanMedium() {
        Medium m=new Buch("Test1","Peter Muster");
        b.addMedium(m);
        Kunde k1=new Kunde("Peter Meier");
        Kunde k2=new Kunde( "Hans Muster");
        b.addKunde(k1);
        b.addKunde(k2);
        b.addMedium(m);
        // Testcase 1 medium is loaned by kunde 1
        assertDoesNotThrow(() -> b.loanMedium(m,k1));
        // Testcase 2 medium is already loaned -> Exception
        assertThrows(AlreadyLoanedException.class,() -> {b.loanMedium(m,k2);});
        // Testcase 3 Check if the loaner is set right and medium is in loans list of Kunde
        assertEquals(true,k1.hasLoan(m));
        assertEquals(k1,m.getLoaner(),"Loaner is not set");
    }

    @Test
    void returnMedium() {
        Medium m = new Buch("Test1", "Peter Muster");
        b.addMedium(m);
        Kunde k = new Kunde("Peter Meier");
        b.addKunde(k);
        b.addMedium(m);
        // Can not return medium not loaned yet
        assertThrows(MediumNotLoanedException.class, () -> b.returnMedium(m));
        // Loan it
        assertDoesNotThrow(() -> b.loanMedium(m, k));
        // Check the return does not throw exception
        assertDoesNotThrow(() -> b.returnMedium(m));
        // check content of medium and kunde
        assertNull(m.getLoaner());
        assertEquals(false,k.hasLoan(m));
    }





    @Test
    void addKunde() {
        Kunde k = new Kunde("Peter Meier");
        b.addKunde(k);
        assertEquals(k,b.getKunde(k.getName()),"Kunde returned is not the same object");
        assertNull(b.getKunde("adfadfasdf"));

    }
}