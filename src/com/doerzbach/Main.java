package com.doerzbach;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Bibliothek b=new Bibliothek();
        Kunde petermeier=new Kunde("Peter Meier");
        Buch buch1=new Buch("Schoener Leben", "Hans Muster");
        b.addMedium(buch1);
        b.addKunde(petermeier);
        try {
            b.loanMedium(buch1,petermeier);
        } catch (AlreadyLoanedException e) {
            System.err.println("Sorry Book is already loened out");
            e.printStackTrace();
        }
    }
}
