package com.doerzbach;

public abstract class Medium {
    private Kunde loaner;
    private String title;
    Medium(String title){
        this.title=title;
    }
    /**
     * checks if the book is already loaned by someone if not sets the loaner and adds the book to the loans of the Kudne
     *
     */
    public void setLoaner(Kunde kunde) throws AlreadyLoanedException{
        if(loaner==null) {
            loaner = kunde;
            kunde.addLoan(this);
        } else {
            throw new AlreadyLoanedException();
        }
    }

    /**
     * This method removes the Book from the loans of the loaner and sets the refernce loaner to null
     */
    public void removeLoaner() throws MediumNotLoanedException {
        if(loaner!=null) {
            loaner.removeMedium(this);
            loaner = null;
        } else throw new MediumNotLoanedException();
    }

    public String getTitle() {
        return title;
    }

    public Object getLoaner() {
        return loaner;
    }
}
