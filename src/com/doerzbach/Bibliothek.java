package com.doerzbach;

import java.util.ArrayList;

public class Bibliothek {
    private ArrayList<Medium> medien=new ArrayList<>();
    private ArrayList<Kunde> kunden=new ArrayList<>();

    public void addMedium(Medium m){
        medien.add(m);
    }
    public void loanMedium(Medium m,Kunde k) throws AlreadyLoanedException {
        m.setLoaner(k);
    }
    public void returnMedium(Medium m) throws MediumNotLoanedException {
        m.removeLoaner();
    }
    public void addKunde(Kunde k){
        kunden.add(k);
    }

    public Medium getMedium(String title) {
        for(Medium m: medien){
            if(m.getTitle()==title){
                return m;
            }
        }
        return null;
    }

    public Object getKunde(String name) {
        for(Kunde k: kunden){
            if(k.getName()==name) return k;
        }
        return null;
    }
}
