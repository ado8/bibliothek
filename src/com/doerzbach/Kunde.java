package com.doerzbach;

import java.util.ArrayList;

public class Kunde {
    ArrayList<Medium> loans = new ArrayList<>();
    String name;

    public Kunde(String name) {
        this.name = name;
    }

    public void addLoan(Medium m) {
        loans.add(m);
    }

    public void removeMedium(Medium medium) {
        loans.remove(medium);
    }

    public boolean hasLoan(Medium m) {
        return loans.contains(m);
    }

    public String getName() {
        return name;
    }
}
