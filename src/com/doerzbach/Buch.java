package com.doerzbach;

public class Buch extends Medium {
    String author;

    public Buch(String title, String author) {
        super(title);
        this.author = author;
    }
}
